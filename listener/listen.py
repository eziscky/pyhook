#!/usr/bin/python3

import tornado.web
import tornado.ioloop
import json
import os
import subprocess
from parser.fields import *


class Listener:
    def __init__(self, port, vals):
        self.vals = vals
        self.port = port
        self.app = tornado.web.Application(
            [
                (r"/", HookHandler, dict(db=vals))
            ]
        )

    def listen(self):
        self.app.listen(self.port)
        tornado.ioloop.IOLoop.current().start()


class HookHandler(tornado.web.RequestHandler):

    def initialize(self, db):
        self.db = db

    def post(self):
        content = json.loads(self.request.body.decode("utf-8"))
        if self.parse(content):
            print("Build Successful")

    def exists(self,name):
        if name not in self.db[TARGETS].keys():
            print("Unsupported project {:s}".format(name))
            return False
        return True

    def build(self, name, url="", ref=""):
        if not self.exists(name):
            return False
        print("-------------------------------")
        print("Building: {:s}".format(name))
        if len(ref) > 0:
            if self.db[TARGETS][name][BRANCH] not in ref.split("/"):
                print("Not watching branch {:s}".format(ref))
                return False
        os.chdir(self.db[TARGETS][name][DIRECTORY])
        if subprocess.call("git pull", shell=True) != 0:
            print("Couldn't pull code")
            return False
        if len(self.db[TARGETS][name][CMD]) > 0:
            if subprocess.call(self.db[TARGETS][name][CMD], shell=True) != 0:
                print("Couldn't execute {:s}".format(self.db[TARGETS][name][CMD]))
                return False
        return True

    def aggregate_downstream(self, downstream):
        aggregated = []
        for project in downstream:
            if not self.exists(project):
                continue
            aggregated.append(project)
            if len(self.db[TARGETS][project][DOWNSTREAM]) > 0:
                if "," in self.db[TARGETS][project][DOWNSTREAM]:
                    for item in self.db[TARGETS][project][DOWNSTREAM].split(","):
                        if self.exists(item):
                            aggregated.append(item)
                else:
                    aggregated.append(self.db[TARGETS][project][DOWNSTREAM])
        return aggregated

    def parse(self, content):
        if not self.build(content["project"]["name"], content["project"]["url"], content["ref"]):
            return False
        if len(self.db[TARGETS][content["project"]["name"]][DOWNSTREAM]) == 0:
            return True
        print("Building downstream projects")
        downstream = []

        if "," not in self.db[TARGETS][content["project"]["name"]][DOWNSTREAM]:
            downstream.append(self.db[TARGETS][content["project"]["name"]][DOWNSTREAM])
        else:
            downstream = self.db[TARGETS][content["project"]["name"]][DOWNSTREAM].split(",")

        aggregated = self.aggregate_downstream(downstream)
        for project in aggregated:
            if not self.build(project):
                print("Failed building downstream project {:s}".format(project))
        return True
