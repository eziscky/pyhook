#!/usr/bin/python3

from parser.parse import *
from listener.listen import *
import os
import sys
import subprocess


def check_content(val):
    try:
        os.stat(val[DIRECTORY])
    except:
        return 1
    if os.path.isfile(val[DIRECTORY]):
        print(val[DIRECTORY], "is not a directory")
        return 0
    if len(os.listdir(val[DIRECTORY])) == 0:
        return 1
    if ".git" in os.listdir(val[DIRECTORY]):
        return 2
    return 1

def daemonize(func,wdir,stdout="/tmp/pyhook.out",stderr="/tmp/pyhook.err"):
    process_id = os.fork()
    if process_id < 0:
        # Fork error.  Exit badly.
        sys.exit(1)
    elif process_id != 0:
        # This is the parent process.  Exit.
        sys.exit(0)
    # This is the child process.  Continue.

    process_id = os.setsid()
    if process_id == -1:
        sys.exit(1)
    
    sout = open(stdout,"w+")
    serr = open(stderr,"w+")
    os.dup2(sout.fileno(),sys.stdout.fileno())
    os.dup2(serr.fileno(),sys.stderr.fileno())   
    
    os.chdir(wdir)
    # Set umask to default to safe file permissions when running
    os.umask(0o027)

    # Create a pid file,uncomment below line to allow only one instance
    lockfile = open(sys.argv[4], 'w')
    # fcntl.lockf(lockfile, fcntl.LOCK_EX|fcntl.LOCK_NB)
    lockfile.write('%s' %(os.getpid()))
    lockfile.flush()
    func()
    
def get_code(val):
    return subprocess.call("git clone -b {:s} {:s} {:s}".format(val[BRANCH], val[URL], val[DIRECTORY]), shell=True)

def print_usage():
    print("./pyhook.py /path/to/conf.toml port [-d /path/to/pidfile]")

def run():
    listener = Listener(port, vals)
    print("Listening on port: {:s}".format(port))
    listener.listen()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print_usage()
        sys.exit(1)
    try:
        os.stat(sys.argv[1])
    except:
        print(sys.argv[1], "not found")
        sys.exit(1)

    parse = Parser(sys.argv[1])
    vals = parse.get_values()
    port = sys.argv[2] if len(sys.argv) > 2 else str(9872)
    daemon = False
    if len(sys.argv) > 3:
        if sys.argv[3] == "-d":
            daemon = True
            
    if len(vals[TARGETS]) == 0:
        print("Nothing to watch")
        sys.exit(0)


    for val in vals[TARGETS].values():
        ret = check_content(val)
        if ret < 1:
            sys.exit(1)
        if ret > 1:
            continue
        ret = get_code(val)
        if ret != 0:
            sys.exit(1)

    if daemon:
        daemonize(run,os.getcwd())
    else:
        run()
