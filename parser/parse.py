#!/usr/bin/python3

import toml


class Parser:
    def __init__(self,conf):
        self.conf = conf
        self.doc = None
        self._load_file()

    def _load_file(self):
        with open(self.conf) as stream:
            try:
                self.doc = toml.load(stream)
            except:
                print("Error parsing toml")
                return

    def get_values(self):
        return self.doc
